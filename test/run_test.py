import unittest
import sys
import os
from pathlib import Path
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(parent)

from test_rel_vec_cloud import *
from test_rel_sca_cloud import *
from test_front_end import *
from test_nonrel_cloud import *

if __name__ == '__main__':
    unittest.main()
